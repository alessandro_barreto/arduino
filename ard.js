function changeStatus(){
	var list = ['status1','status2','status3','status4']
	for(status of list){
		createStatus(status)
	}
}

function createStatus(id){
	var status = document.getElementById(id).innerHTML;
	if(status === "1"){
		document.getElementById(id).innerHTML="1";
		var idUrl = '/?off'+id
		document.getElementById('btn'+id).innerHTML="<a href='"+idUrl+"' class='shadow mt-5 btn btn-warning btn-lg'><img src='https://img.filipeflop.com/files/download/automacao/lampada_ligada.png' alt='Responsive image' style='width: 25px;height: 25px;'>Corrente Ligada</a>";
	}else{
		var idUrl = '/?on'+id
		document.getElementById(id).innerHTML="0";
		document.getElementById("btn"+id).innerHTML="<a href='"+idUrl+"' class='shadow mt-5 btn btn-light  btn-lg'><img src='https://img.filipeflop.com/files/download/automacao/lampada_desligada.png' alt='Responsive image' style='width: 25px;height: 25px;'> Corrente Desligada</a>";
	}
}